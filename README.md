### Local Installation
- First, make sure you have Docker and NPM installed on your computer. NPM will be used to compile the front-end assets.
- second, run `cp .env.example .env` and add your credentials
- Then to install the project locally, make sure Docker is up and run this command: `sh install.sh`
