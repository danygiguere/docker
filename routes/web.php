<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Http;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    $response = Http::withHeaders([
//            'user-agent' => 'test',
//            'x-forwarded-for' => 'test',
//    ])->get('http://docker.test');
//    return $response;
    return view('welcome');
});
