#!/bin/bash
#This script is for local installation only

#export .env vars for this script
if [ -f .env ]; then
  export $(cat .env | sed 's/#.*//g' | xargs)
fi

npm install
npm run dev

docker build -t php:7.4-fpm .

docker-compose down || exit
docker-compose up -d

sleep 10 #Give mysql 10sec to start up before droping database
docker-compose exec db mysql -uroot -p${DB_PASSWORD} -e "DROP DATABASE IF EXISTS ${DB_DATABASE};"
sleep 10 #Give mysql 10sec to drop database before creating a new one
docker-compose exec db mysql -uroot -p${DB_PASSWORD} -e "CREATE DATABASE ${DB_DATABASE};"

docker-compose exec app composer install
docker-compose exec app php /var/www/artisan key:generate
docker-compose exec app php /var/www/artisan migrate:fresh --seed

docker-compose down || exit

docker-compose up
